FROM alpine:3

# Label args.
ARG IMAGE_AUTHORS
ARG IMAGE_DESCRIPTION=''
ARG IMAGE_LICENSE='Unknown'
ARG IMAGE_PROJECT_URL
ARG IMAGE_TITLE
ARG IMAGE_VENDOR
ARG IMAGE_VERSION
ARG IMAGE_REVISION

# DNS map, comma separated values, each of one in format: DOMAIN[:ADDRESS],
# where ADDRESS is optional and defaults to 127.0.0.1
ENV DNSMASQ_DNS_MAP="local,dev.local,lo,dev.lo,test"

# Fetch and configure dnsmasq.
RUN set -eux; \
  apk --no-cache add dnsmasq=2.86-r0; \
  rm -rf /var/cache/apk/*; \
  mkdir -p /etc/default/; \
  echo 'ENABLED=1' >> /etc/default/dnsmasq; \
  echo 'IGNORE_RESOLVCONF=yes' >> /etc/default/dnsmasq;

COPY dnsmasq.conf /etc/dnsmasq.conf

# Configure entrypoint and default cmd.
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN ["chmod", "+x", "/docker-entrypoint.sh"]
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["dnsmasq", "--no-daemon"]

LABEL \
  org.opencontainers.image.authors="${IMAGE_AUTHORS}" \
  org.opencontainers.image.description="${IMAGE_DESCRIPTION}" \
  org.opencontainers.image.licenses="${IMAGE_LICENSE}" \
  org.opencontainers.image.source="${IMAGE_PROJECT_URL}" \
  org.opencontainers.image.title="${IMAGE_TITLE}" \
  org.opencontainers.image.vendor="${IMAGE_VENDOR}" \
  org.opencontainers.image.version="${IMAGE_VERSION}" \
  org.opencontainers.image.revision="${IMAGE_REVISION}"
